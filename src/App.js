import React, { useState } from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import { v4 as uuid } from 'uuid';

import Todos from './components/Todos';
import About from './screens/About';

import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';

function App() {

    const [ todoList, setTodoList ] = useState([
        {
            id: uuid(), 
            title: 'Take out the trash', 
            isCompleted: false
        },
        {
            id: uuid(), 
            title: 'Dinner with family', 
            isCompleted: false
        }, 
        {
            id: uuid(), 
            title: 'Meeting with boss', 
            isCompleted: false
        }
    ]);

    const markComplete = (id) => {
        setTodoList( todoList.map( (todo) => {
                if(todo.id === id){
                    todo.isCompleted = !todo.isCompleted
                }
                return todo;
            })
        )
    }

    const delTodo = (id) => {
        setTodoList( todoList.filter(todo => todo.id !== id ))
    }

    const addTodo = (title) => {
        setTodoList([
            ...todoList, 
            {
                id: uuid(),
                title: title,
                isCompleted: false
            }
        ])
    }

    return (
        <Router>
            <Switch>
                <Route path='/about'>
                    <About />
                </Route>
                <Route path='/'>
                    <Todos todoList={todoList} markComplete={markComplete} delTodo={delTodo} addTodo={addTodo} />
                </Route>
            </Switch>
        </Router>
    );
}

export default App;
