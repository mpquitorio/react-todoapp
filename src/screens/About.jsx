import React from 'react';
import Header from '../components/Header';

function About() {
    return(
        <div className='container'>
            <Header />
            <h3 className='mt-3'>About Us</h3>
            <hr/>
            <p>This is the Todo App version 1.0. It is part of the React course.</p>
        </div>
    );
}

export default About;