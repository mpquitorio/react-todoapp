import React from 'react';
import { Link } from 'react-router-dom';

function Header(){

    const linkStyle = {
        textDecoration: 'none', 
        color: '#fff'
    }

    return(
        <header className='bg-dark p-3 text-white text-center'>
            <h1 className='text-uppercase'>Todo App</h1>
            <Link style={linkStyle} to='/'>Home</Link> | <Link style={linkStyle} to='/about'>About</Link>
        </header>
    );
}

export default Header;