import React, { useState } from 'react';
import Header from './Header';

function Todos( {todoList, markComplete, delTodo, addTodo} ) {

    const [ todoTitle, setTodoTitle ] = useState('');

    const handleInput = (e) => {
        setTodoTitle( e.target.value )
    }

    const handleSubmit = (e) => {
        e.preventDefault();
        addTodo(todoTitle);
        setTodoTitle('');
    }

    return (
        <div className='container'>

            <Header />

            <form className='d-flex form-group my-3' onSubmit={handleSubmit}>
                <input type='text' name='title' placeholder='Add Todo ...' className='form-control mr-1' value={todoTitle} onChange={handleInput} />
                <input type='submit' value='Submit' className='btn btn-primary' />
            </form>

            <table className='table'>
                <thead>
                    <tr>
                        <th width='5%'></th>
                        <th width='90%'>List</th>
                        <th width='5%'>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    { 
                        todoList.map( (todo) => {

                            const getStyle = {
                                textDecoration: todo.isCompleted ? 'line-through' : 'none'
                            }

                            return (
                                <tr key={todo.id}>
                                    <td><input type='checkbox' onChange={markComplete.bind(this, todo.id)} /></td>
                                    <td style={getStyle}>{ todo.title }</td>
                                    <td className='py-2 text-center'><input type='button' className='btn btn-danger py-1' value='x' onClick={delTodo.bind(this, todo.id)} /></td>
                                </tr>
                            );
                        })
                    }
                </tbody>
            </table>
        </div>
    );
}

export default Todos;
